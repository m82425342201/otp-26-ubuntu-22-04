vsn=$1
root=/workspace/otp-26-ubuntu-22-04
sudo rm -rf /usr/local/lib/erlang
sudo tar xvf $root/otp-26-0.tar.xz -C /usr
cp -r $root/.cache $HOME
sudo cp $root/rebar3 /usr/local/bin
sudo rm -rf /usr/local/lib/elixir
sudo tar xvf $root/elixir-$vsn.tar.xz -C /usr
